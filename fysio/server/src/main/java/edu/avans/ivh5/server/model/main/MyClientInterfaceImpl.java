/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.avans.ivh5.server.model.main;

import java.rmi.RemoteException;
import org.apache.log4j.Logger;
import edu.avans.ivh5.api.FysioClientIF;

/**
 * @author Robin Schellius
 */
public class MyClientInterfaceImpl implements FysioClientIF {

	// Get a logger instance for the current class
	static Logger logger = Logger.getLogger(MyClientInterfaceImpl.class);

	// When a member is found, we cache it in a hashmap for fast retrieval in a
	// next search.
	//	private HashMap<Integer, Member> members;

	// The factory that provides DAO instances for domain classes.
	//	private DAOFactory daoFactory; 

	// The servicename that identifies this service in the RMI registry.
	// When finding all available services in the registry we want to exclude
	// ourselves.
	private String myServicename;

	/**
	 * Instantiate and initialize the server stub. The servicename is the name
	 * that identifies this server in the RMI registry. It is received from the
	 * server and stored locally for later lookup. When communicating beween
	 * multiple servers we can thus distinguish ourselves from the remote
	 * server.
	 * 
	 * @param servicename
	 *            The name that identifies this server in the RMI registry.
	 */
	public MyClientInterfaceImpl(String servicename) throws RemoteException {

		logger.debug("Constructor using " + servicename);

		// Our 'own' servicename; prevents looking it up in the registry as a
		// remote server.
		myServicename = servicename;

		try {
			// Getting DAOFactory for local and remote data access
			//			daoFactory = DAOFactory.getDAOFactory(Settings.props.getProperty(Settings.propDataStore));
		} catch (NoClassDefFoundError e) {
			logger.fatal("Error: Class not found! (Is it in the propertyfile?)");
			logger.fatal(e.getMessage());
		}
	}

	public String doClientMethod() throws RemoteException {
		// TODO Auto-generated method stub
		return null;
	}

	
	
}
