/**
 * Avans Academie voor ICT & Engineering
 * Worked example - Library Information System.
 */
package edu.avans.ivh5.client.control;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.rmi.RemoteException;
import java.util.EventListener;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import org.apache.log4j.Logger;
import edu.avans.ivh5.api.FysioClientIF;
import edu.avans.ivh5.client.view.ui.UserInterface;
import edu.avans.ivh5.shared.model.domain.DomainClassOne;

/**
 * <p>
 * This class contains the controller functionality from the Model-View-Control approach. 
 * The controller handles all actions and events that result from interaction with the system, 
 * in whatever way - being user interaction via the UI, or machine-to-machine interaction via
 * a data access object.
 * </p>
 * <p>
 * The controller separates the controlling functionality from the model (managing access to data)
 * and the view (managing the displaying of data).
 * </p>
 * 
 * @author Robin Schellius
 */
public class Controller implements ActionListener, EventListener, ListSelectionListener {

	// Get a logger instance for the current class
	static Logger logger = Logger.getLogger(Controller.class);

	// A reference to the user interface for access to view components (text fields, tables)
	private UserInterface userinterface = null;
	
	// Reference to the member manager.
	private FysioClientIF manager = null;
	
	// When we find members on a host and service, we store them here for later retrieval.
	// ArrayList<RemoteMemberInfo> globalMemberList = null;
	
	/**
	 * Constructor, initializing generally required references to user interface components.
	 */
	public Controller(FysioClientIF mgr) 
	{
		logger.debug("Constructor");
		manager = mgr;
	}
	
	/**
	 * Set the link to the user interface, so we can display the results of actions.
	 * 
	 * @param userinterface
	 */
	public void setUIRef(UserInterface ui) {
		this.userinterface = ui;
	}


	/**
	 * Find the member identified by membershipNr and display its information.
	 * 
	 * @param hostname Name/IP address of the host to find the member on.
	 * @param servicename Name of the service to find the host on.
	 * @param membershipNr Number of the manager to be found.
	 */
	public void doFindMember(String host, String service,int membershipNr) 
	{
		logger.debug("doFindMember " + host + " " + service + " " + membershipNr);

		//
		// Dit is de plek waar de client een domain class gebruikt.
		// In de IVH5 proftaak gebruik je je eigen domainclasses.
		// 
		DomainClassOne myDomainClass; 	// Member member;
				
		
		if (manager == null || userinterface == null) {
			logger.error("Manager or userinterface is null!");
		} else {
			try {
				//
				// Hier wil je waarschijnlijk iets anders dan een String terug ...
				// Misschien iets als: ArrayList<DomainClass> = manager.doClientMethod();
				//
				String result =	manager.doClientMethod();

				if (result != null) {
					// Doe iets met het result
					// userinterface.setMemberDetails(member);
				} else {
					logger.debug("Fout !");
					userinterface.setStatusText("Member " + membershipNr + " not found.");
				}
			} catch (RemoteException e) {
				logger.error("Error: " + e.getMessage());
			}
		}
	}


	/**
	 * <p>
	 * Performs the corresponding action for a button. This method can handle
	 * actions for each button in the window. The method itself selects the
	 * appropriate handling based on the ActionCommand that we have set on each button. 
	 * </p>
	 * <p>
	 * Whenever a new button is added to the UI, provide it with an ActionCommand name and 
	 * provide the appropriate handling here.
	 * </p>
	 * 
	 * @param ActionEvent The event that is fired by the JVM whenever an action occurs.
	 */
	public void actionPerformed(ActionEvent e) {
	
		if (e.getActionCommand().equals("FIND_MEMBER")) {
			/**
			 * Find Member is called when the user inserts a 
			 * membershipNr and presses Search.
			 */
			try {	
				// 
				// Hier voer je vanuit de client de acties uit die bij de FIND_MEMBER event horen.
				// In de proftaak van IVH5 zoek je geen members maar klanten, of verzekeringen, of ...
				//
			} catch (NumberFormatException ex) {
				logger.error("Wrong input, only numbers allowed");
				userinterface.setStatusText("Wrong input, only numbers allowed.");
				userinterface.setSearchBoxText("");
			}
		} else if (e.getActionCommand().equals("FIND_ALL_MEMBERS")) {
			/**
			 * Do a search for all members on all available services, and 
			 * display them in the table.
			 */
			try {
				logger.debug("Find all members on all hosts/services");
				// doFindAllMembers();			
			} catch (Exception ex) {
				logger.error("Error: " + ex.getMessage());
				ex.printStackTrace();
			}
		}
	}

	/**
	 * This method handles list selection events. Whenever the user selects a row in the memberlist
	 * of the user interface, valueChanged is fired and the appropriate action is taken. 
	 */
	public void valueChanged(ListSelectionEvent e) {
		
	    ListSelectionModel lsm = (ListSelectionModel)e.getSource();
	
	    // There can be multiple events going on, since the user could possibly select multiple
	    // rows and even individual columns. We first wait until the user is finished adjusting. 
	    if(e.getValueIsAdjusting() == false) {
	
	    	// There could be multiple rows (or columns) selected, even 
	    	// though ListSelectionModel = SINGLE_SELECTION. Find out which 
	    	// indexes are selected.
	        int minIndex = lsm.getMinSelectionIndex();
	        int maxIndex = lsm.getMaxSelectionIndex();
	        for (int i = minIndex; i <= maxIndex; i++) {
	            if (lsm.isSelectedIndex(i)) 
	            {
	                String selectedMember = (String) userinterface.getDataTableModel().getValueAt(i, 0);
	                String hostname = (String) userinterface.getDataTableModel().getValueAt(i, 3);
	                String servicename = (String) userinterface.getDataTableModel().getValueAt(i, 4);
	                
	                if (selectedMember != null && !selectedMember.equals("")) {                        
	                    logger.debug("Selected member = " + selectedMember);
	                    doFindMember(hostname, servicename, Integer.parseInt(selectedMember));
	                }
	            }
	        }
	    }
	}
}