package edu.avans.ivh5.api;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * 
 * @author Robin Schellius
 *
 */
public interface FysioClientIF extends Remote {

	/**
	 */
	public String doClientMethod() throws RemoteException;

}
